﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
   public GameObject panelMenu, panelLevel, panelSettings;

   public void Play()
   {
      panelLevel.SetActive(true);
      panelSettings.SetActive(false);
      panelMenu.SetActive(false);
   }
   public void LevelOne()
   {
	   SceneManager.LoadScene(1);
   }
     public void LevelTwo()
   {
	   SceneManager.LoadScene(2);
   }
     public void LevelThree()
   {
	   SceneManager.LoadScene(3);
   }
     public void Quit()
   {
      Application.Quit();
   } 
     public void Settings()
	{
		panelLevel.SetActive(false);
		panelSettings.SetActive(true);
		panelMenu.SetActive(false);
	}
     public void Back()
     {
	     panelLevel.SetActive(true);
	     panelSettings.SetActive(false);
	     panelMenu.SetActive(false);
     }
}
