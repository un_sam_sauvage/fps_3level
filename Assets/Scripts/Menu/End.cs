﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour
{
   public void LevelOne() // Load LevelOne
   {
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
      Time.timeScale = 1f;
      SceneManager.LoadScene(1);
   }
   public void LevelTwo()// Load LevelTwo
   {
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
      Time.timeScale = 1f;
      SceneManager.LoadScene(2);
   }
   public void LevelThree()// Load LevelThree
   {
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
      Time.timeScale = 1f;
      SceneManager.LoadScene(3);
   }
   public void LevelMenu()// Load Menu
   {
      Cursor.lockState = CursorLockMode.None;
      Cursor.visible = true;
      Time.timeScale = 1f;
      SceneManager.LoadScene(0);
   }
   public void Quit()
   {
      Time.timeScale = 1f;
      Application.Quit();
   }
}
