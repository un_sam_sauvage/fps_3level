﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    private bool gameIsPaused ;
    public GameObject panelPause;
    private void Update()
    {
        //if you press the pause button it will enable the panel and freeze the time
        if (Input.GetKeyDown(KeyCode.Escape) &&!gameIsPaused )
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            panelPause.SetActive(true);
            gameIsPaused = true;
            Time.timeScale = 0;
        }
        //if you press the pause button and it's already pause it will be unpaused
        else if(Input.GetKeyDown(KeyCode.Escape) &&gameIsPaused )
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            panelPause.SetActive(false);
            gameIsPaused = false;
            Time.timeScale = 1f;
        }
    }

    public void Resume()
    {
        panelPause.SetActive(false);
        gameIsPaused = false;
        Time.timeScale = 1;
    }
}
