﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
   public float timer;
   public int timerInt;
   public TextMeshProUGUI timerText;
   public int enemy;
   public GameObject panelWin, panelTime, player;
   public Camera secondCam;
   void Update() // transform the timer into an int to see only the number before the , 
   {
      timer -= Time.deltaTime;
      timerText.text = timerInt.ToString();
      timerInt = (int)Math.Ceiling(timer);

      if (timer <= 30)
      {
         timerText.color = Color.red;
      }

      if (enemy <= 0) // unlock panel win
      {
         panelWin.SetActive(true);
         panelTime.SetActive(false);
         player.SetActive(false);
         secondCam.enabled = true;
         Cursor.lockState = CursorLockMode.None;
         Cursor.visible = true;
      }
   }

   public void NumberEnemy()
   {
      enemy--;
   }
}
