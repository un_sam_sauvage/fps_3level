﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectBulletOnButton : MonoBehaviour
{
 public GameObject particleSystem;
//when the bullet enter in the button it will enable the particle system
 private void OnCollisionEnter(Collision other)
 {
     if (other.gameObject.CompareTag("Bullet")) 
     {
        particleSystem.SetActive(true);
     }
 }

 private void OnTriggerEnter(Collider other)
 {
     if (other.gameObject.CompareTag("Bullet"))
     {
         particleSystem.SetActive(true);
     }
 }
}
