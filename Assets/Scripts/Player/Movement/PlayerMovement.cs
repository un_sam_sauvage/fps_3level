﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.81f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public float jumpHeigth = 3f;

    private Vector3 velocity;

    private bool isGrounded;
 
    void Update() // Check if the ground is under the player to avoid infinite jump
    {
        Walk();
        Jump();
    }

    public void Walk()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        float x = Input.GetAxis("Horizontal"); // Allows to the player to walk 
        float z = Input.GetAxis("Vertical");
        
        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);
    }

    public void Jump() // Allows to the player to jump
    {
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeigth * -1.5f * gravity);
        }
        velocity.y += gravity * Time.deltaTime * 2f;

        controller.Move(velocity * Time.deltaTime);
    }
}
