﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeManager : MonoBehaviour
{
    public int life;
    public Slider slidelife;
    public GameObject panelEnd, panelTime;
    public Camera secondCamera;
    public List<GameObject> enemyToKill = new List<GameObject>();
    // Update is called once per frame
    void Update()
    {
        slidelife.value = life;
        if (life <= 0) //end game
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            secondCamera.enabled = true;
            panelEnd.SetActive(true);
            panelTime.SetActive(false);
            foreach (var ene in enemyToKill)
            {
                Destroy(ene);
                Debug.Log("Je rentre dans la liste");
            }    
            Destroy(gameObject);
        }

        if (Input.GetKeyDown(KeyCode.M)) // add life to the player
        {
            life = 999999;
        }
    }

    public void DamagePlayer()
    {
        life--;
    }
}
