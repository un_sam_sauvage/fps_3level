﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShootPlayer : MonoBehaviour
{
    public GameObject firePoint;
    public GameObject camera;
    private Pooling _pooling;

    public int maxAmmo = 10;
    public int currentAmmo = -1;
    public float reloadTime = 1f;
    private bool _isReloading = false;

    public TextMeshProUGUI ammoText, reloadText;
    // Start is called before the first frame update
    //Get the instance from the pooling so it can call the fonction from the script
    void Start()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera");
        _pooling = Pooling.instance;
        if(currentAmmo == -1) 
            currentAmmo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        ammoText.text = currentAmmo.ToString();
        if (Input.GetMouseButtonDown(0) && currentAmmo >= 1)
        {
            OnShoot();
        }
        if (_isReloading)
            return;
        
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            reloadText.text = "Reloading ...";
            return;
        }
    }

    IEnumerator Reload()
    {
        _isReloading = true;
        Debug.Log("Reloading...");
        
        yield return new WaitForSeconds(reloadTime);
        
        currentAmmo = maxAmmo;
        _isReloading = false;
        reloadText.text = "";
    }
    //when the player press the Fire button instantiate a bullet from the pool
    public void OnShoot()
    {
        currentAmmo--;
        _pooling.SpawnFromPool("Bullet",firePoint.transform.position ,camera.transform.rotation);
    }
}
