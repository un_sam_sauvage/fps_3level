﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    //make the bullet go forward
    private void Update()
    {
        transform.position += transform.forward * 25 * Time.deltaTime;
        if (transform.position.y < -10 || transform.position.y > 20)
        {
            gameObject.SetActive(false);
        }
    }
//when it enter in collision with something set it false
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("hit");
        if (!other.gameObject.CompareTag("PlayerDetector"))
        {        
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyLife>().EnemyDamage();
        }
    }
}
