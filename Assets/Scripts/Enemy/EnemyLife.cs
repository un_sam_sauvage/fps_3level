﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLife : MonoBehaviour
{
    public int life;

    public GameObject gameManager;

    public Material colorEnemy;
    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {
            gameManager.GetComponent<GameManager>().NumberEnemy();
            Destroy(gameObject);
        }
    }

    public void EnemyDamage()
    {
        life--;
        StartCoroutine(ChangeColor());
    }

    IEnumerator ChangeColor()
    {
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach( Renderer renderer in renderers)
        {
            renderer.material.color = Color.red;
        }
        yield return new WaitForSeconds(0.2f);
        foreach( Renderer renderer in renderers)
        {
            renderer.material.color = colorEnemy.color;
        }
    }
}
