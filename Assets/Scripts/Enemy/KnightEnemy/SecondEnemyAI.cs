﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SecondEnemyAI : MonoBehaviour
{
    
    private Animator _anim;

    public GameObject sword;
    public bool launchAnim, canMove;
    private NavMeshAgent _nav;
    private Transform _player;
    
    // Start is called before the first frame update
    void Awake()
    {
        _anim = sword.GetComponent<Animator>();
        canMove = false;
    }

    private void Start()
    {
        _nav = GetComponent<NavMeshAgent>();
        _player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (launchAnim)
        {
            _anim.Play("Knight_anim");
        }

        if (canMove)
        {
            _nav.SetDestination(_player.position);
        }
    }
}
