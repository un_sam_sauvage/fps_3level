﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMovementDetector : MonoBehaviour
{
    public GameObject enemyKnight;
    private void OnTriggerEnter(Collider other) // Stop movement 
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enemyKnight.GetComponent<SecondEnemyAI>().canMove = false;
        }
    }private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enemyKnight.GetComponent<SecondEnemyAI>().canMove = false;
        }
    }
    private void OnTriggerExit(Collider other) // can Move
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enemyKnight.GetComponent<SecondEnemyAI>().canMove = true;
        }
    }
    
}
