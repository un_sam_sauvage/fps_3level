﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
   private void OnTriggerEnter(Collider other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         other.GetComponent<LifeManager>().DamagePlayer();
         Debug.Log("Player touch Sword");
      }
   }
}
