﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetectorKnight : MonoBehaviour
{
   public GameObject enemyKnight;
   private void OnTriggerEnter(Collider other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         enemyKnight.GetComponent<SecondEnemyAI>().launchAnim = true;
         enemyKnight.GetComponent<SecondEnemyAI>().canMove = true;
      }
   }
   private void OnTriggerStay(Collider other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         enemyKnight.GetComponent<SecondEnemyAI>().launchAnim = true;
         enemyKnight.GetComponent<SecondEnemyAI>().canMove = true;
      }
   }
   private void OnTriggerExit(Collider other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         enemyKnight.GetComponent<SecondEnemyAI>().launchAnim = false;
         enemyKnight.GetComponent<SecondEnemyAI>().canMove = false;
      }
   }
}
