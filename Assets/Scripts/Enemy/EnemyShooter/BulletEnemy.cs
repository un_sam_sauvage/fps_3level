﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
	public float speed;

	public float lifeTimer;
    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;  // Add speed and life to bullet
        lifeTimer -= Time.deltaTime;
        if (lifeTimer <= 0f) {
	        Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) // Check if the bullet should be destroyed.
    {
	    if (other.gameObject.CompareTag("Player"))
	    {
		    other.GetComponent<LifeManager>().DamagePlayer();
		    Destroy(gameObject);
	    }
    }
}
