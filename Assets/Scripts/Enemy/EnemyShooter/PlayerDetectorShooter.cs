﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetectorShooter : MonoBehaviour
{
    public GameObject enemy;
    public void OnTriggerEnter(Collider other) // Launch the fonction OnDetect
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enemy.GetComponent<FirstEnemyAi>().OnDetect();
            Debug.Log("Detect Player");
        }
    }

    public void OnTriggerStay(Collider other) // Launch the fonction isPlayerDetect
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enemy.GetComponent<FirstEnemyAi>().isPlayerDetect = true;
            Debug.Log(enemy.GetComponent<FirstEnemyAi>().isPlayerDetect);
        }
    }

    public void OnTriggerExit(Collider other) // Launch the fonction CantInvoke
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enemy.GetComponent<FirstEnemyAi>().CantInvoke();
        }
    }
}
