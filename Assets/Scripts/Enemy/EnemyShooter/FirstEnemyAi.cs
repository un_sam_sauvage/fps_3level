﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstEnemyAi : MonoBehaviour
{
    public Transform player;
    public bool isPlayerDetect = false;
    public GameObject bulletenemy;
    public Transform firePoint;
    public ParticleSystem fire;
    public void Update()
    {
        if (isPlayerDetect)
        {
            transform.LookAt(player);
        }
    }
    public void OnDetect() // Start Shoot
    {
        InvokeRepeating("Shoot", 1, 1);
    }

    private void Shoot() // Allows to the player to shoot 
    {
        Instantiate(bulletenemy, firePoint.position, firePoint.rotation);
        Instantiate(fire, firePoint.position, firePoint.rotation);
    }

    public void CantInvoke() // Stop the shooting 
    {
        CancelInvoke(); 
        isPlayerDetect = false;
        Debug.Log("Stop Shooting");
    }
}
